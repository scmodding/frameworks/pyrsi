=======
History
=======

0.1.18 (2021-01-11)
-------------------

* Added Hangar support


0.1.16 (2020-12-30)
-------------------

* Moved to GitLab

0.1.15 (2020-12-23)
-------------------

* Added new Roadmap API

0.1.11 (2020-05-02)
-------------------

* Added RSI status page API

0.1.2 (2018-12-27)
------------------

* Added Authentication and session management required for accessing privileged Organization members
  list

0.1.0 (2018-12-10)
------------------

* Initial commit
